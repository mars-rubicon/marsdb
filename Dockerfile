FROM redis:latest
RUN apt-get update &&\
  apt-get install -y git-core python-pip --fix-missing

RUN pip install Flask redis websockets

RUN cd /opt &&\
  git clone https://gitlab.com/mars-rubicon/marsdb.git &&\
  cd /opt/marsdb

WORKDIR /opt/marsdb

RUN redis-server /opt/marsdb/redis.conf &
CMD python /opt/marsdb/db.py
